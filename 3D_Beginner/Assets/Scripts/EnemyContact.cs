﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyContact : MonoBehaviour
{
    public GameObject player;
    public PlayerController pc;
    public int hitpoints = 5;


    private void Update()
    {
        transform.LookAt(player.transform);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == player)
        {
            SceneManager.LoadScene(1);
        }
    }
}