﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LootContainer : MonoBehaviour
{
    // Variables
    [SerializeField] string lootMessage;
    bool isSearched = false;
    bool inRange = false;

    // References
    [SerializeField] TextMeshProUGUI searchText;
    [SerializeField] TextMeshProUGUI lootText;
    public GameObject lootItem;
    [SerializeField] PlayerController player;

    void Update()
    {
        if (Input.GetButtonDown("Interact") && inRange)
        {
            Loot();
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            searchText.GetComponent<TextMeshProUGUI>().enabled = true;
            inRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        searchText.enabled = false;
        lootText.enabled = false;
        inRange = false;
    }

    void Loot()
    {
        if (!isSearched)
        {
            lootText.text = lootMessage;
            if (lootItem.name == "FinalKey")
            {
                player.hasFinalKey = true;
            }
            else if (lootItem.name == "shotgun")
            {
                player.inventory.Add(lootItem);
            }
            else if (lootItem.name == "ammo_shotgun")
            {
                player.shotgunAmmo += 2;
                player.UpdateHUD();
            }
            else if (lootItem.name == "ammo_pistol")
            {
                player.handgunAmmo += 5;
                player.UpdateHUD();
            }
            else if (lootItem.name == "grenade")
            {
                player.inventory.Add(lootItem);
            }
            else if (lootItem.name == "pistol")
            {
                player.inventory.Add(lootItem);
            }
            isSearched = true;
            player.EquipFirstWeapon();
        }
        else
        {
            lootText.text = "You find nothing";
        }
        searchText.enabled = false;
        lootText.enabled = true;
    }
}