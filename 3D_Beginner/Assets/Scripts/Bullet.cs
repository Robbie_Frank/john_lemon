﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] float velocity;
    Rigidbody rb;
    public bool isShotgun;
    // Start is called before the first frame update
    void Start()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * velocity;
    }

    void Update()
    {

    }

    private void FixedUpdate()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy" && isShotgun)
        {
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.GetComponent<EnemyContact>().hitpoints -= 1;
            if (collision.gameObject.GetComponent<EnemyContact>().hitpoints <= 0)
            {
                Destroy(collision.gameObject);
            }
        }
        Destroy(gameObject);
    }
}