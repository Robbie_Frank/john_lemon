﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class EscapeMansionDoor : MonoBehaviour
{
    bool inRange = false;
    bool doorUnlocked = false;
    float timer = 0;

    [SerializeField] TextMeshProUGUI searchText;
    [SerializeField] TextMeshProUGUI lootText;
    [SerializeField] PlayerController player;

    void Update()
    {
        if (Input.GetButtonDown("Interact") && inRange)
        {
            AttemptEscape();
        }
        if (doorUnlocked)
        {
            timer += Time.deltaTime;
            if (timer >= 5f)
            {
                EscapeTheMansion();
                Debug.Log("Application.Quit called");
            }
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            searchText.GetComponent<TextMeshProUGUI>().enabled = true;
            inRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        searchText.enabled = false;
        lootText.enabled = false;
        inRange = false;
    }

    void AttemptEscape()
    {
        if (!player.hasFinalKey)
        {
            lootText.text = "You must find the key to unlock this door and escape the mansion!";
        }
        else
        {
            lootText.text = "You have escaped the mansion with your life!";
            doorUnlocked = true;
        }
        searchText.enabled = false;
        lootText.enabled = true;
    }

    void EscapeTheMansion()
    {
        Application.Quit();
    }
}