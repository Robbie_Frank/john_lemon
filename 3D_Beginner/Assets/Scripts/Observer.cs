﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Observer : MonoBehaviour
{
    public Transform player;
    public NavMeshAgent navMeshAgent;
    public PlayerController pc;
    bool m_IsPlayerInRange;
    public bool chasingPlayer = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }

    private void Update()
    {
        if (chasingPlayer)
        {
            navMeshAgent.SetDestination(player.position);
            Debug.Log("Still chasing player");
        }
        if (m_IsPlayerInRange)
        {
            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;
            if (Physics.Raycast(ray, out raycastHit))
            {
                Debug.Log("Raycast Hit Player");
                if (raycastHit.collider.transform == player)
                {
                    chasingPlayer = true;
                    Debug.Log("Start Chasing Player");
                }
            }
        }
    }
}