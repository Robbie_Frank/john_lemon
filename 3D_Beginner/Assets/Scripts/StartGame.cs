﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    private void Start()
    {
        Time.timeScale = 1;
    }
    public void BeginGame()
    {
        SceneManager.LoadScene(1);
    }
}