﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Variables
    [SerializeField] float walkSpeed;
    [SerializeField] float strafeSpeed;
    [SerializeField] float sprintSpeed;
    [SerializeField] float lookSensitivity;
    [SerializeField] int health;
    float currentTilt = 0f;
    public bool hasFinalKey = false;
    public List<GameObject> inventory;
    public GameObject equippedWeapon;
    public int shotgunAmmo = 0;
    public int handgunAmmo = 0;
    public int grenades = 0;


    // References
    [SerializeField] Camera camera;
    [SerializeField] TextMeshProUGUI weaponText;
    [SerializeField] TextMeshProUGUI weaponCurrentMagText;
    [SerializeField] TextMeshProUGUI weaponTotalSpareAmmoText;
    [SerializeField] GameObject weaponPlacement;
    [SerializeField] GameObject bullet;
    [SerializeField] AudioSource footsteps;

    void Start()
    {
        // Locks the cursor
        Cursor.lockState = CursorLockMode.Locked;
        // Sets the location of the cursor to the middle of the screen.
        Cursor.SetCursor(null, new Vector2(0, 0), CursorMode.Auto);
    }

    void Update()
    {
        Aim();
        Move();
        if (Input.GetButtonDown("Fire1"))
        {
            FireWeapon();
        }
        if (Input.GetButtonDown("Reload"))
        {
            Reload(equippedWeapon.GetComponent<Weapon>());
        }
        if (Input.GetButtonDown("Sprint"))
        {
            walkSpeed += 1f;
        }
        if (Input.GetButtonUp("Sprint"))
        {
            walkSpeed -= 1f;
        }
        if (Input.GetButtonDown("Weapon1"))
        {
            if (inventory[0] != null)
            {
                ChangeWeapon(0);
            }
        }
        if (Input.GetButtonDown("Weapon2"))
        {
            if (inventory[1] != null)
            {
                ChangeWeapon(1);
            }
        }
    }

    private void FixedUpdate()
    {
        Move();
    }
    void Move()
    {
        float x = Input.GetAxis("Horizontal") * strafeSpeed * Time.deltaTime;
        float z = Input.GetAxis("Vertical") * walkSpeed * Time.deltaTime;

        transform.Translate(x, 0, z);
        if (x != 0 || z != 0)
        {
            if (!footsteps.isPlaying)
            {
                footsteps.Play();
            }
        }
        else
        {
            footsteps.Stop();
        }
    }

    void Aim()
    {
        // Gets mouse input information
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
        // Rotates player left and right according to mouse input
        transform.Rotate(Vector3.up, mouseX * lookSensitivity);
        // Controls the up and down tilt of the camera attached to the player
        currentTilt -= mouseY * lookSensitivity;
        // Makes sure the camera cannot look up or down past 90 degrees
        currentTilt = Mathf.Clamp(currentTilt, -90, 90);
        // Rotates the camera upwards and downwards according to mouse input
        camera.transform.localEulerAngles = new Vector3(currentTilt, 0, 0);
    }

    void FireWeapon()
    {
        if (equippedWeapon != null && equippedWeapon.GetComponent<Weapon>().currentMagazineCount > 0)
        {
            GameObject projectile = Instantiate(bullet, camera.transform.position + transform.forward *2f, camera.transform.rotation);
            if (equippedWeapon.name == "shotgun")
            {
                projectile.GetComponent<Bullet>().isShotgun = true;
            }
            equippedWeapon.GetComponent<Weapon>().currentMagazineCount -= 1;
            UpdateHUD();
        }
    }

    public void EquipFirstWeapon()
    {
        if (inventory.Count == 1)
        {
            equippedWeapon = inventory[0];
            weaponText.text = equippedWeapon.GetComponent<Weapon>().weaponName;
            equippedWeapon.GetComponent<Weapon>().currentMagazineCount = equippedWeapon.GetComponent<Weapon>().magazineCapacity;
            weaponCurrentMagText.text = equippedWeapon.GetComponent<Weapon>().currentMagazineCount.ToString();
            if (equippedWeapon.name == "shotgun")
            {
                weaponTotalSpareAmmoText.text = shotgunAmmo.ToString();
            }
            else
            {
                weaponTotalSpareAmmoText.text = handgunAmmo.ToString();
            }
            GameObject gunInHand = Instantiate(equippedWeapon, weaponPlacement.transform.position, weaponPlacement.transform.rotation) as GameObject;
            gunInHand.transform.parent = camera.transform;
        }
    }

    public void UpdateHUD()
    {
        weaponCurrentMagText.text = equippedWeapon.GetComponent<Weapon>().currentMagazineCount.ToString();
        if (equippedWeapon.name == "shotgun")
        {
            weaponTotalSpareAmmoText.text = shotgunAmmo.ToString();
        }
        else
        {
            weaponTotalSpareAmmoText.text = handgunAmmo.ToString();
        }
    }

    void Reload(Weapon currentWeapon)
    {
        if (currentWeapon.name == "shotgun" && shotgunAmmo > 0 && currentWeapon.currentMagazineCount < currentWeapon.magazineCapacity)
        {
            currentWeapon.currentMagazineCount += 1;
            shotgunAmmo -= 1;
            UpdateHUD();
        }
        else if (currentWeapon.name == "pistol" && handgunAmmo > 0 && currentWeapon.currentMagazineCount < currentWeapon.magazineCapacity)
        {
            currentWeapon.currentMagazineCount += 1;
            handgunAmmo -= 1;
            UpdateHUD();
        }
    }

    void ChangeWeapon(int weapon)
    {
        equippedWeapon = inventory[weapon];
        weaponText.text = equippedWeapon.GetComponent<Weapon>().weaponName;
        UpdateHUD();
    }
}